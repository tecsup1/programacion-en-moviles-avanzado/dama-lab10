
import UIKit
import AVFoundation

class SoundViewController: UIViewController {

    @IBOutlet weak var grabarButton: UIButton!
    @IBOutlet weak var reproducirButton: UIButton!
    @IBOutlet weak var nombreTextField: UITextField!
    @IBOutlet weak var agregarButton: UIButton!
    @IBOutlet weak var duacionLabel: UILabel!
    
    
    var grabarAudio : AVAudioRecorder?
    var reproducirAudio : AVAudioPlayer?
    var audioURL : URL?
    
    var botonPresionado : String = ""
    
    // probando timer:
    var timer:Timer!
    
    @IBAction func grabarTapped(_ sender: Any) {
        if grabarAudio!.isRecording{
            // detener la grabación
            grabarAudio?.stop()
            // cambiar texto del botón grabar
            grabarButton.setTitle("Grabar", for: .normal)
            reproducirButton.isEnabled = true
            agregarButton.isEnabled = true
        }else{
            // empezar a grabar
            grabarAudio?.record()
            // cambiar el texto del botón grabar a detener
            grabarButton.setTitle("DETENER", for: .normal)
            reproducirButton.isEnabled = false
            // probando timer:
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
            botonPresionado = "grabando"
        }
    }
    
    @IBAction func reproducirTapped(_ sender: Any) {
        do{
            try reproducirAudio = AVAudioPlayer(contentsOf: audioURL!)
            reproducirAudio!.play()
            print("Reproduciendo audiooo")
            botonPresionado = "reproduciendo"
            //print("duracioooon!!")
            //print(reproducirAudio!.duration)
        }catch{}
    }
    
  
    @objc func updateTime() {
        var currentTime = 0
        if(botonPresionado == "grabando"){
            currentTime = Int(grabarAudio!.currentTime)
        }else if(botonPresionado == "reproduciendo"){
            currentTime = Int(reproducirAudio!.currentTime)
        }
        let minutes = currentTime/60
        let seconds = currentTime - minutes * 60
            
        duacionLabel.text = String(format: "%02d:%02d", minutes,seconds) as String
    }
    
    @IBAction func agregarTapped(_ sender: Any) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let grabacion = Grabacion(context: context)
        grabacion.nombre = nombreTextField.text
        grabacion.audio = NSData(contentsOf: audioURL!)! as Data
        grabacion.duracion = reproducirAudio!.duration
        (UIApplication.shared.delegate as! AppDelegate).saveContext ()
        navigationController!.popViewController(animated: true)
        print("duracion")
        print(grabacion.duracion)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurarGrabacion()
        
        reproducirButton.isEnabled = false
        
        agregarButton.isEnabled = false
    }
    
    func configurarGrabacion(){
        do{
            // creando sesión de audio
            let session = AVAudioSession.sharedInstance()
            try session.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.default, options:[])
            try session.overrideOutputAudioPort(.speaker)
            try session.setActive(true)
            
            // creando dirección para el archivo de audio
            let basePath:String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
            let pathComponents = [basePath, "audio.m4a"]
            audioURL = NSURL.fileURL(withPathComponents: pathComponents)!
            
            // impresión de ruta donde se guardan los archivos
            print("***************************")
            print(audioURL!)
            print("***************************")
            
            // crear opciones para el grabador de audio
            var settings:[String:AnyObject] = [:]
            settings[AVFormatIDKey] = Int(kAudioFormatMPEG4AAC) as AnyObject?
            settings[AVSampleRateKey] = 44100.0 as AnyObject?
            settings[AVNumberOfChannelsKey] = 2 as AnyObject?

            // crear el objeto de grabación de audio
            grabarAudio = try AVAudioRecorder(url: audioURL!, settings: settings)
            grabarAudio!.prepareToRecord()
        }catch let error as NSError{
            print(error)
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
